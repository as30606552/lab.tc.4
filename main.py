import numpy as np
import bitarray as ba
import random
from scripts.golay_code import GolayCode
from scripts.rm_code import RMCode

if __name__ == '__main__':

    # строим код Голея
    code = GolayCode()
    print()

    # проверка работоспособности метода исправления ошибок кода Голея (сравнение с примерами из лекций)

    # декодируем вектор: 101.111.101.111 010.010.010.010, должны получить: 001.111.101.110
    input_array = np.array([1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0])
    output_array = code.decode(input_array)
    print('Задание 4.1.3. Пример 1. 101.111.101.111 010.010.010.010 -> 001.111.101.110')
    print('Входной массив:', input_array)
    print('Выходной массив:', output_array)
    print()
    # декодируем вектор: 001.001.001.101 101.000.101.000, должны получить: 001.001.011.111
    input_array = np.array([0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0])
    output_array = code.decode(input_array)
    print('Задание 4.1.3. Пример 2. 001.001.001.101 101.000.101.000 -> 001.001.011.111')
    print('Входной массив:', input_array)
    print('Выходной массив:', output_array)
    print()

    # проверка генерации порождающей матрицы кода Рида-Маллера (сравнение с примерами из лекций)

    # строим матрицу: G(0,1) = [1 1]
    code = RMCode(0, 1)
    code.generate_G()
    print('Задание 4.2.2. Порождающая матрица. Пример 1. G(0,1) = [1 1]')
    print('Построенная матрица:')
    print(code.G)
    print()
    # строим матрицу: G(1,1) = [1 1
    #                           0 1]
    code = RMCode(1, 1)
    code.generate_G()
    print('Задание 4.2.2. Порождающая матрица. Пример 2. G(1,1) = [1 1')
    print('                                                        0 1]')
    print('Построенная матрица:')
    print(code.G)
    print()
    # строим матрицу: G(1,2) = [1 1 1 1
    #                           0 1 0 1
    #                           0 0 1 1]
    code = RMCode(1, 2)
    code.generate_G()
    print('Задание 4.2.2. Порождающая матрица. Пример 3. G(1,2) = [1 1 1 1')
    print('                                                        0 1 0 1')
    print('                                                        0 0 1 1]')
    print('Построенная матрица:')
    print(code.G)
    print()
    # строим матрицу: G(2,2) = [1 1 1 1
    #                           0 1 0 1
    #                           0 0 1 1
    #                           0 0 0 1]
    code = RMCode(2, 2)
    code.generate_G()
    print('Задание 4.2.2. Порождающая матрица. Пример 4. G(2,2) = [1 1 1 1')
    print('                                                        0 1 0 1')
    print('                                                        0 0 1 1')
    print('                                                        0 0 0 1]')
    print('Построенная матрица:')
    print(code.G)
    print()
    # строим матрицу: G(0,3) = [1 1 1 1 1 1 1 1]
    code = RMCode(0, 3)
    code.generate_G()
    print('Задание 4.2.2. Порождающая матрица. Пример 5. G(0,3) = [1 1 1 1 1 1 1 1]')
    print('Построенная матрица:')
    print(code.G)
    print()
    # строим матрицу: G(1,3) = [1 1 1 1 1 1 1 1
    #                           0 1 0 1 0 1 0 1
    #                           0 0 1 1 0 0 1 1
    #                           0 0 0 0 1 1 1 1]
    code = RMCode(1, 3)
    code.generate_G()
    print('Задание 4.2.2. Порождающая матрица. Пример 6. G(1,3) = [1 1 1 1 1 1 1 1')
    print('                                                        0 1 0 1 0 1 0 1')
    print('                                                        0 0 1 1 0 0 1 1')
    print('                                                        0 0 0 0 1 1 1 1]')
    print('Построенная матрица:')
    print(code.G)
    print()
    # строим матрицу: G(2,3) = [1 1 1 1 1 1 1 1
    #                           0 1 0 1 0 1 0 1
    #                           0 0 1 1 0 0 1 1
    #                           0 0 0 1 0 0 0 1
    #                           0 0 0 0 1 1 1 1
    #                           0 0 0 0 0 1 0 1
    #                           0 0 0 0 0 0 1 1]
    code = RMCode(2, 3)
    code.generate_G()
    print('Задание 4.2.2. Порождающая матрица. Пример 7. G(2,3) = [1 1 1 1 1 1 1 1')
    print('                                                        0 1 0 1 0 1 0 1')
    print('                                                        0 0 1 1 0 0 1 1')
    print('                                                        0 0 0 1 0 0 0 1')
    print('                                                        0 0 0 0 1 1 1 1')
    print('                                                        0 0 0 0 0 1 0 1')
    print('                                                        0 0 0 0 0 0 1 1]')
    print('Построенная матрица:')
    print(code.G)
    print()
    # строим матрицу: G(3,3) = [1 1 1 1 1 1 1 1
    #                           0 1 0 1 0 1 0 1
    #                           0 0 1 1 0 0 1 1
    #                           0 0 0 1 0 0 0 1
    #                           0 0 0 0 1 1 1 1
    #                           0 0 0 0 0 1 0 1
    #                           0 0 0 0 0 0 1 1
    #                           0 0 0 0 0 0 0 1]
    code = RMCode(3, 3)
    code.generate_G()
    print('Задание 4.2.2. Порождающая матрица. Пример 8. G(3,3) = [1 1 1 1 1 1 1 1')
    print('                                                        0 1 0 1 0 1 0 1')
    print('                                                        0 0 1 1 0 0 1 1')
    print('                                                        0 0 0 1 0 0 0 1')
    print('                                                        0 0 0 0 1 1 1 1')
    print('                                                        0 0 0 0 0 1 0 1')
    print('                                                        0 0 0 0 0 0 1 1')
    print('                                                        0 0 0 0 0 0 0 1]')
    print('Построенная матрица:')
    print(code.G)
    print()

    # строим код Рида-Маллера
    code = RMCode(1, 3)
    code.generate_G()
    code.generate_Hs()

    # проверка работоспособности метода исправления ошибок кода Рида-Маллера (сравнение с примерами из лекций)

    # декодируем вектор: 10101011, должны получить: 1100
    input_array = np.array([1, 0, 1, 0, 1, 0, 1, 1])
    output_array = code.decode(input_array)
    print('Задание 4.2.3. Пример 1. 10101011 -> 1100')
    print('Входной массив:', input_array)
    print('Выходной массив:', output_array)
    print()
    # декодируем вектор: 10001111, должны получить: 0001
    input_array = np.array([1, 0, 0, 0, 1, 1, 1, 1])
    output_array = code.decode(input_array)
    print('Задание 4.2.3. Пример 2. 10001111 -> 0001')
    print('Входной массив:', input_array)
    print('Выходной массив:', output_array)
    print()

    # кодирование и декодирование файла

    # кодируем файл file.txt, записываем результат в файл encoded.txt
    with open('./data/file.txt', 'rb') as input_file:
        with open('./data/encoded.txt', 'wb') as output_file:
            code.encode_file(input_file, output_file)
    # добавляем в закодированный файл encoded.txt ошибку, результат записываем в файл spoiled.txt
    file_bytes = ba.bitarray()
    with open('./data/encoded.txt', 'rb') as file:
        file_bytes.fromfile(file)
        random.seed()
        random_position = random.randint(0, len(file_bytes) - 1)
        file_bytes[random_position] = not file_bytes[random_position]
    with open('./data/spoiled.txt', 'wb') as file:
        file_bytes.tofile(file)
    # декодируем файл spoiled.txt с обнаружением ошибок, результат записываем в файл decoded.txt
    with open('./data/spoiled.txt', 'rb') as input_file:
        with open('./data/decoded.txt', 'wb') as output_file:
            code.decode_file(input_file, output_file)
    print('Посмотрите файлы file.txt и decoded.txt, их содержимое должно совпадать')
