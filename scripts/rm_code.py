from functools import reduce
import numpy as np
from scripts.utils import C, H, convert_integer_to_binary_list
import bitarray as ba


class RMCode:

    # конструктор
    def __init__(self, r, m):
        self.r = r
        self.m = m
        # число информационных (исходных) разрядов
        # сумма i от 0 до r чисел сочетаний из m по i
        self.k = reduce(lambda x, y: x + y, map(lambda i: C(m, i), range(r + 1))) if r != 0 else 0
        # длина закодированного сообщения
        self.n = 2 ** m
        # порождающую матрицу оставляем пустой, генерацию выносим в отдельный метод
        self.G = None
        # список матриц H для декодирования, генерацию выносим в отдельный метод
        self.Hs = []
        # количество битов, добавленных к битам, считанным из файла, чтобы суммарное количество делилось на k нацело
        self.__added_bits = None

    # генерация порождающей матрицы, делегирует выполнение приватной функции
    def generate_G(self):
        self.G = self.__generate_G(self.r, self.m)

    # генерация порождающий матрицы кода Рида-Маллера с произвольными параметрами
    # для рекурсивного вызова
    def __generate_G(self, r, m):
        # G(0,m) = [11..1]
        if r == 0:
            return np.ones((1, 2 ** m), dtype=int)
        # G(m,m) = [ G(m-1,m)
        #             0..01   ]
        if r == m:
            # строим вектор из нужного количества нулей и на конце единица
            e = np.zeros((1, 2 ** m), dtype=int)
            e[0][-1] = 1
            return np.concatenate([self.__generate_G(m - 1, m), e])
        # G(r,m) = [ G(r,m-1)  G(r,m-1)
        #             00..0   G(r-1,m-1) ]
        G1 = self.__generate_G(r, m - 1)
        G2 = self.__generate_G(r - 1, m - 1)
        upper = np.concatenate([G1, G1], axis=1)
        lower = np.concatenate([np.zeros(G2.shape, dtype=int), G2], axis=1)
        return np.concatenate([upper, lower])

    # кодирование сообщения посредством умножения на матрицу G
    def encode(self, a):
        return (a @ self.G) % 2

    # построение списка матриц H для декодирования
    def generate_Hs(self):
        # пробегаемся по i от 1 до m
        for i in range(1, self.m + 1):
            self.Hs += [H(self.m, i)]

    # декодирование сообщения
    def decode(self, w):
        # заменим все 0 в w на -1
        ww = w.copy()
        for i in range(len(ww)):
            if ww[i] == 0:
                ww[i] = -1
        # умножаем w последовательно на все H из списка
        for h in self.Hs:
            ww = ww @ h
        # поиск индекса максимального по модулю элемента в w
        j = 0
        max_elem = ww[0]
        for i, elem in enumerate(ww):
            if abs(elem) > abs(max_elem):
                max_elem = elem
                j = i
        # преобразуем j в двоичное представление
        vj = convert_integer_to_binary_list(j)
        # дополняем vj нулями до длины k-1
        vj += [0 for _ in range(self.k - 1 - len(vj))]
        # добавлем в начало единицу в случае, если w[j]>0, и ноль в противном случае
        vj = [1] + vj if max_elem > 0 else [0] + vj
        return np.array(vj)

    # кодирование файла
    def encode_file(self, input_file, output_file):
        # считываем биты из файла в массив
        file_bytes = ba.bitarray()
        file_bytes.fromfile(input_file)
        file_list = file_bytes.tolist()
        # добавляем к массиву битов необходимое количество, чтобы размер массива делился нацело на k
        rem = len(file_list) % self.k
        if rem != 0:
            self.__added_bits = self.k - rem
            file_list.extend(ba.bitarray(self.__added_bits))
        # превращаем считанные биты в массив массивов по k элементов в каждом
        bit_arrays = np.array(file_list, dtype=int).reshape(-1, self.k)
        # кодируем каждые k бит из файла и записываем в выходной файл
        result_array = ba.bitarray()
        for k_bits in bit_arrays:
            result_array.extend(self.encode(k_bits))
        result_array.tofile(output_file)

    # декодирование файла
    def decode_file(self, input_file, output_file):
        # считываем биты из файла в массив
        file_bytes = ba.bitarray()
        file_bytes.fromfile(input_file)
        file_list = file_bytes.tolist()
        # при записи в файл сообщение было дополнено нулями до целого количества байт, отсеиваем лишние биты
        rem = len(file_list) % self.n
        if rem != 0:
            file_list = file_list[:-rem]
        # превращаем считанные биты в массив массивов по n элементов в каждом
        bit_arrays = np.array(file_list, dtype=int).reshape(-1, self.n)
        result_array = ba.bitarray()
        # декодируем каждые n бит
        for n_bits in bit_arrays:
            result_array.extend(self.decode(n_bits))
        # отсеиваем лишние биты, добавленные при кодировании
        if self.__added_bits is not None:
            result_array = result_array[:-self.__added_bits]
        # записываем декодированное сообщение в файл
        result_array.tofile(output_file)


if __name__ == '__main__':
    code = RMCode(1, 3)
    code.generate_G()
    code.generate_Hs()
    a = np.array([0, 1, 0, 0])
    print('a =', a)
    w = code.encode(a)
    print('w =', w)
    print('decode =', code.decode(w))
