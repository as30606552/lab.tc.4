from functools import reduce


# вес Хемминга - количество ненулевых компонент вектора
import numpy as np


def wt(a):
    return reduce(lambda x, y: int(x) + int(y), a)


# число сочетаний из n по k
def C(n, k):
    if 0 <= k <= n:
        nn = 1
        kk = 1
        for t in range(1, min(k, n - k) + 1):
            nn *= n
            kk *= t
            n -= 1
        return nn // kk
    else:
        return 0


# K^n (декартова степень), где K = {0,1}
def K(n):
    # K^0 = K
    if n == 0:
        return np.array([[0], [1]])
    # K^n = {(K^n-1, 0), (K^n-1, 1)}
    first_part = K(n - 1)
    last_part = np.copy(first_part)
    for vec in first_part:
        np.append(vec, 0)
    for vec in last_part:
        np.append(vec, 1)
    return np.concatenate([first_part, last_part])


# произведение Кронекера: A * B = [a{i,j}*B]
def Kronecker_multiplication(A, B):
    # строим пустой двумерный массив с количеством столбцов равным произведению числа столбцов в A и в B
    result = np.empty((0, A.shape[1] * B.shape[1]), dtype=int)
    for row in A:
        # строим пустой двумерный массив с количеством строк как у B
        row_result = np.empty((B.shape[0], 0), dtype=int)
        for elem in row:
            row_result = np.concatenate([row_result, elem * B], axis=1)
        result = np.concatenate([result, row_result])
    return result


# H = [1  1
#      1 -1]
H11 = np.array([[1, 1], [1, -1]])


# H(m,i) = I(2^(m-i)) * H * I(2^(i-1)
def H(m, i):
    return Kronecker_multiplication(
        Kronecker_multiplication(
            np.eye(2 ** (m - i), dtype=int),
            H11
        ),
        np.eye(2 ** (i - 1), dtype=int)
    )


# преобразование целого числа в двоичный список
def convert_integer_to_binary_list(i):
    if i == 0:
        return [0]
    if i == 1:
        return [1]
    result = []
    while i != 1:
        result += [i % 2]
        i //= 2
    return result + [1]
